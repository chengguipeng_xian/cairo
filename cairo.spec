%global cairogl --disable-gl

Name:           cairo
Version:        1.16.0
Release:        3
Summary:        A 2D graphics library
License:        LGPLv2 or MPLv1.1
URL:            http://cairographics.org
Source0:        http://cairographics.org/releases/%{name}-%{version}.tar.xz

Patch0001:	0001-Set-default-LCD-filter-to-FreeType-s-default.patch
Patch0002:	0002-ft-Use-FT_Done_MM_Var-instead-of-free-when-available.patch
Patch0003:	0003-cairo-composite_color_glyphs.patch
Patch0004:	0004-cff-Allow-empty-array-of-operands-for-certain-operat.patch
Patch6000:	CVE-2019-6461.patch
Patch6001:	CVE-2019-6462.patch

BuildRequires:  pkgconfig glib2-devel librsvg2-devel
BuildRequires:  libXrender-devel libX11-devel libpng-devel libxml2-devel
BuildRequires:  pixman-devel >= 0.30.0
BuildRequires:  freetype-devel >= 2.1.9
BuildRequires:  fontconfig-devel >= 2.2.95
Provides:       cairo-gobject
Obsoletes:      cairo-gobject

%description
Cairo is a 2D graphics libarary with support for multiple output devices.
It provides high-quality display and print output and this package also
contains functionality to make cairo graphics library integrate well with
GObject used by GNOME.

%package        devel
Summary: Development files for cairo
Requires: %{name}%{?_isa} = %{version}-%{release}
Provides: cairo-gobject-devel cairo-tools
Obsoletes: cairo-gobject-devel cairo-tools

%description    devel
This package contains libraries, header files and developer documentation
needed for developing software which uses the cairo graphics library and
cairo GObject library and contains tools for working with the cairo graphics
library as well.

%prep
%autosetup -p1

%build
%configure --disable-static --enable-xlib --enable-ft --enable-ps  \
           --enable-pdf --enable-svg --enable-tee --enable-gobject \
           %{cairogl} --disable-gtk-doc
sed -i -e 's/^hardcode_libdir_flag_spec=.*/hardcode_libdir_flag_spec=""/g' \
-e 's/^runpath_var=LD_RUN_PATH/runpath_var=DIE_RPATH_DIE/g' libtool
%make_build V=1

%install
%make_install
find $RPM_BUILD_ROOT -name '*.la' -delete

%files
%license COPYING COPYING-LGPL-2.1 COPYING-MPL-1.1
%doc AUTHORS BIBLIOGRAPHY BUGS NEWS README
%{_libdir}/libcairo.so.*
%{_libdir}/libcairo-script-interpreter.so.*
%{_bindir}/cairo-sphinx
# This is GObject relative lib file
%{_libdir}/libcairo-gobject.so.*

%files          devel
%doc ChangeLog PORTING_GUIDE
%dir %{_includedir}/cairo/
%exclude %{_includedir}/cairo/cairo-gl.h
%{_includedir}/cairo/*.h
%{_libdir}/*.so
%exclude %{_libdir}/pkgconfig/cairo-glx.pc
%exclude %{_libdir}/pkgconfig/cairo-gl.pc
%exclude %{_libdir}/pkgconfig/cairo-egl.pc
%{_libdir}/pkgconfig/*.pc
# These two files are development tools related
%{_libdir}/cairo/
%{_datadir}/gtk-doc/html/cairo
# This file is development tools related
# cairo-trace: Record cairo library calls for later playback
%{_bindir}/cairo-trace

%changelog
* Sun Sep 27 2020 wangye <wangye70@huawei.com> - 1.16.0-3
- fix source URL

* Fri Sep 18 2020 yanglu <yanglu60@huawei.com> - 1.16.0-2
- Type:cves
- ID:CVE-2019-6461 CVE-2019-6462
- SUG:NA
- DESC:fix CVE-2019-6461 CVE-2019-6462

* Mon Jul 13 2020 jinzhimin <jinzhimin2@huawei.com> - 1.16.0-1
- Version upgrade

* Fri Jan 10 2020 zhangrui <zhangrui182@huawei.com> - 1.15.14-3
- Remove unnecessary patch

* Thu Sep 19 2019 Alex Chao <zhaolei746@huawei.com> - 1.15.14-2
- Package init
